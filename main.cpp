#include "minesweeper.h"
#include <iostream>

using namespace std;

int main() {

	int h , b , n;
	char choice;

	cout << "Welcome to MineSweeper!" << endl;
	cout << "This program generates and solves a mine-sweeper puzzle as per the requirement." << endl;
	cout << "Press G/g for generating a puzzle , or S/s to solve a given puzzle." << endl; 
	cin >> choice;

	cout << "Please enter the height of the grid." << endl;
	cin >> h;

	cout << "Please enter the breadth of the grid." << endl;
	cin >> b;

	cout << "Please enter the number of mines in the grid." << endl;
	cin >> n;

	if(choice == 'S' || choice == 's')
	{
		mines mine1(h , b , n);
		mine1.run();
	}
	else if(choice == 'G' || choice == 'g')
	{
		mines mine1(h , b , n);
		mine1.generate();
	}
	
	cout << "Thank you for using MineSweeper!" << endl;

	return 0;

}
