#ifndef MINES_H
#define MINES_H

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <string>
#include <cstring>
using namespace std;

class mines{

	private:
	
	//Variables and data structures
	char **grid;	//Array to store the gaming-grid
	char **play_field;	//Array where the current states are being stored
	int height, breadth, number, flags ;	
	bool fail;
 	bool change, solving_generated;

	//Functions
	void take_input();
	void neighbour_opener(int i, int j);	//Opens all empty spaces in surrounding regions( up , down , left , right ) recursively
	void display();	//Displpays the current state of the grid; Helps follow the path taken by computer to solve it
	void random_select();	//The function that , incase of no possible moves , randomly opens a cell
	void failure();	//Function that is called , if by luck , the program fails to solve
	void check_surroundings(int a, int b); // To check surroundings of an opened cell, and update accordingly 

	void ask_user(); //Asks user to tell which cell to open in case of no-possible moves
	void open_cell(int i, int j);	//Opens a cell
	void fill_neighbour(int i , int j);	//Fills the neighbour of a mine with numbers in case of generation	

	public:

	mines(int height, int breadth, int mine);	//Constructor to create the grid of the mines
	void run();		
	void generate();

};

#endif
