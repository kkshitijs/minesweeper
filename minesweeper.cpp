#include "minesweeper.h"
#include <unistd.h>

mines::mines(int height1, int breadth1 , int mine) {

	height = height1;
	breadth = breadth1;
	number = mine;
	flags = 0;
	fail = false;
	solving_generated = false;
	grid = new char*[height1];
	play_field = new char*[height1];

	for (int i = 0; i < height1; i++)
	{

		grid[i] = new char[breadth1];
		play_field[i] = new char[breadth1];		

	}

}

void mines::take_input() {

	cout << "Please enter the grid in standard format. Refer Readme for the format." << endl;
	
	for (int i = 0; i < height; i++)
	{

		for(int j = 0; j < breadth; j++)
		{

			cin >> grid[i][j];
			play_field[i][j] = 'B';
		}
	
	}

}

void mines::neighbour_opener(int i, int j) {

	check_surroundings(i,j);

	if( i > 0 && play_field[i-1][j] != 'W') 
	{
		if(grid[i-1][j] == 'E') {
			play_field[i-1][j] = 'W';
			neighbour_opener(i-1 , j);		
		}		
		else if(grid[i-1][j] != 'M')
		{
			play_field[i-1][j] = grid[i-1][j];
	  	check_surroundings(i-1,j);
	  }
	}

	if( i < height - 1 && play_field[i+1][j] != 'W') 
	{
		if(grid[i+1][j] == 'E') {
			play_field[i+1][j] = 'W';
			neighbour_opener(i+1 , j);
		}
		else if(grid[i+1][j] != 'M'){
			play_field[i+1][j] = grid[i+1][j];		
	    check_surroundings(i+1,j);
	  }
	}

	if( j > 0 && play_field[i][j-1] != 'W') 
	{
		if(grid[i][j-1] == 'E') {	
			play_field[i][j-1] = 'W';
			neighbour_opener(i , j-1);
		}
		else if(grid[i][j-1] != 'M'){
			play_field[i][j-1] = grid[i][j-1];
	  	check_surroundings(i,j-1);
	  }
	}

	if( j < breadth - 1 && play_field[i][j+1] != 'W') 
	{
		if(grid[i][j+1] == 'E') {
			play_field[i][j+1] = 'W';
			neighbour_opener(i , j+1);
		}
		else if(grid[i][j+1] != 'M'){
			play_field[i][j+1] = grid[i][j+1];
	  	check_surroundings(i,j+1);
	  }
	}

	if( i < height - 1 && j < breadth - 1 && play_field[i+1][j+1] != 'W') 
	{
		if(grid[i+1][j+1] == 'E') {
			play_field[i+1][j+1] = 'W';
			neighbour_opener(i+1 , j+1);
		}
		else if(grid[i+1][j+1] != 'M'){
			play_field[i+1][j+1] = grid[i+1][j+1];
	  	check_surroundings(i+1,j+1);
	  }
	}

	if( i > 0 && j < breadth - 1 && play_field[i-1][j+1] != 'W') 
	{
		if(grid[i-1][j+1] == 'E') {
			play_field[i-1][j+1] = 'W';
			neighbour_opener(i-1 , j+1);
		}
		else if(grid[i-1][j+1] != 'M'){
			play_field[i-1][j+1] = grid[i-1][j+1];
	  	check_surroundings(i-1,j+1);
	  }
	}

	if( j > 0 && i > 0 && play_field[i-1][j-1] != 'W') 
	{
		if(grid[i-1][j-1] == 'E') {	
			play_field[i-1][j-1] = 'W';
			neighbour_opener(i-1 , j-1);
		}
		else if(grid[i-1][j-1] != 'M'){
			play_field[i-1][j-1] = grid[i-1][j-1];
	  	check_surroundings(i-1,j-1);
	  }
	}

	if( j > 0 && i < height - 1 && play_field[i+1][j-1] != 'W') 
	{
		if(grid[i+1][j-1] == 'E') {	
			play_field[i+1][j-1] = 'W';
			neighbour_opener(i+1 , j-1);
		}
		else if(grid[i+1][j-1] != 'M'){
			play_field[i+1][j-1] = grid[i+1][j-1];
	  	check_surroundings(i+1,j-1);
	  }
	}

	
}

void mines::check_surroundings(int a, int b){
  
  int count=0;
  int countmines=0;
  //cout << "row "<< a << " col " << b <<endl;
  if (!((int)play_field[a][b] > 48 && (int)play_field[a][b] < 57)) return;
  //cout << "row "<< a << " col " << b << " val " << play_field[a][b]<<endl; 
  
  for (int i=a-1; i<a+2; i++)
  {
  
    for (int j=b-1; j<b+2; j++)
    {
    
      if (!(i<0 || j<0 || i>=height || j>=breadth || (i==a && j==b)))
      {
        if (play_field[i][j] == 'B') count++;
        if (play_field[i][j] == 'F') countmines++;
      }
      
    } 
      
  }
//  cout << count << " " << countmines<<endl;
  if (count+countmines==(int)(play_field[a][b]) - 48) 
  {
  
    for (int i=a-1; i<a+2; i++)
    {
    
      for (int j=b-1; j<b+2; j++)
      {
      
        if (!(i<0 || j<0 || i>=height || j>=breadth || (i==a && j==b) ))
        {
        
          if (play_field[i][j] == 'B') 
          {
          
            cout << "Flag the cell at row " << i+1 << " and column "<<  j+1 << "."<<endl;
            play_field[i][j]='F';
            change=true;
            flags++;
            display();
          }
          
        }   
        
      }
      
    }
    
  }
  
  else if (countmines==(int)play_field[a][b] -48) 
  {
  
    for (int i=a-1; i<a+2; i++)
    {
  
      for (int j=b-1; j<b+2; j++)
      {
      
        if (!(i<0 || j<0 || i>=height || j>=breadth || (i==a && j==b)))
        {
        
          if (play_field[i][j] == 'B') 
          {
            cout << "Open the cell at row " << i+1 << " and column " << j+1 << ".";
            open_cell(i,j);
             
          }
        
        }
        
      } 
      
    }
  
  }

}



void mines::display() {
 // string default_console = "\033[0m";
  
	system("clear");
  //string color = "\033[0;30m";
	cout << "The grid at this stage is : " << endl;
  //cout << "\033[1;31mbold red text\033[0m\n";
	for (int i = 0; i < height; i++)
	{

		for(int j = 0; j < breadth; j++)
		{
      if (play_field[i][j]=='B'){
        cout << "\033[1;37mO \033[0m";
      }
      else if (play_field[i][j]=='W'){
        cout << "\033[1;34m+ \033[0m";
      }
      
      else if (play_field[i][j]=='F'){
        cout << "\033[1;31m$ \033[0m";
      }
      else if (play_field[i][j]=='*'){
        cout << "\033[1;31m* \033[0m";
      }
      else {
        string k="\033[1;32m";
        
        k.push_back(play_field[i][j]);
        k.append(" \033[0m");
        cout << k ;
//        cout << "\033[1;32m\033[0m";
      }
    			//cout << play_field[i][j] << " ";
		}
		
		cout << endl;	

	}

	usleep(50000);
}

void mines::run() {

	srand( time(NULL) );
	
	if(!solving_generated) take_input();
	display();
	cout << "No smart moves possible. Please tell if you want me to randomly select a cell , or else input a cell to open . " << endl;
	cout << "Press Y for random selection / N for user-input" << endl;
	
	char a;
	int i , j;
	cin >> a;

	if( a == 'Y' || a == 'y' )	random_select();
	else
	{
		cout << "Enter row number to select : " << endl;
		cin >> i;
		cout << "Enter column number to select : " << endl;
		cin >> j;

		open_cell(i - 1, j - 1);
	}
	while (1){
  if(flags == number) {
    cout << "Solved!" << endl;
    break;
  }
	else if(fail)	{
	  cout << "A mine blew up.\nFailed to solve. :(" << endl;
	  break;
	}
	else ask_user();
  }
}

void mines::random_select() {

	int i,j;
  	do{
	int random1 = rand();
	i = random1 % height;
	int random2 = rand();
	j = random2 % breadth;
	}while(play_field[i][j] != 'B');	

	cout << "Randomly I opened the cell at row " << i+1 << " column " << j+1 << "."<<endl;
	open_cell(i , j);
	

}

void mines::failure() {

	for (int i = 0; i < height; i++)
	{

		for(int j = 0; j < breadth; j++)
		{

			if(grid[i][j] == 'M' && play_field[i][j] != 'F')	play_field[i][j] = '*';
		}
	
	}

	fail = true;
	display();

}

void mines::ask_user() {
  
  do{
    change=false;
  
    for (int j=0; j< height; j++)
    {
     
      for (int k=0; k<breadth ; k++)
      {
  
        check_surroundings(j,k);
  
      }
  
    } 
  } while (change==true);

	if(flags == number) return; 
	cout << "No smart moves possible. Please tell if you want me to randomly select a cell , or else input a cell to open . " << endl;
	cout << "Press Y for random selection / N for user-input" << endl;
	
	char a;
	int i , j;
	cin >> a;

	if( a == 'Y' || a == 'y' )	random_select();
	else
	{
		cout << "Enter row number to select : " << endl;
		cin >> i;
		cout << "Enter column number to select : " << endl;
		cin >> j;

		open_cell(i - 1, j - 1);
	}

}	

void mines::open_cell(int i , int j) {
  change=true;
	if (grid[i][j] == 'E')
	{
		play_field[i][j] = 'W';
		neighbour_opener(i,j);
		display();
	}

	else if (grid[i][j] == 'M')
	{
		failure();
	}

	else if (play_field[i][j] != 'B')
	{
		cout << "Already Opened. Wrong Choice!" << endl;
		ask_user();	
	}
	else 
	{
		play_field[i][j] = grid[i][j];
		display();
		check_surroundings(i, j);
		for (int a=i-1; a<i+2; a++){
		  for (int b=j-1; b<j+2; b++){
		    if (!(a<0 || b<0 || a>=height || b>=breadth || (a==i && b==j))){
		      check_surroundings(a,b);
		    }
		  }
		}
		
	}

}

void mines::generate() {

	srand( time(NULL) );
	int count = 0 , i , j;
	
	for(i = 0; i < height; i++)
	{
		for(j = 0; j < breadth; j++)
		{
			grid[i][j] = '#';
		}
	} 

	while(count != number)
	{
		i = rand() % height;
		j = rand() % breadth;
		if(grid[i][j] != 'M') 
		{
			grid[i][j] = 'M';
			count++;
		}
	}

	for(i = 0; i < height; i++)
	{
		for(j = 0; j < breadth; j++)
		{
			if(grid[i][j] == 'M') fill_neighbour(i , j);
		}
	}

	for(i = 0; i < height; i++)
	{
		for(j = 0; j < breadth; j++)
		{
			if(grid[i][j] == '#') grid[i][j] = 'E';
		}
	}

	cout << "Grid Generated!" << endl;
	
	for (int i = 0; i < height; i++)
	{

		for(int j = 0; j < breadth; j++)
		{

			cout << grid[i][j] << " ";
			play_field[i][j] = 'B';
		}
		
		cout << endl;	

	}

	cout << "Do you want to solve this generated grid now ? (Y/N)" << endl;
	char choice;
	cin >> choice;
	
	solving_generated = false;
	if(choice == 'Y' || choice == 'y')	
	{
		solving_generated = true;
		run();	
	}

}

void mines::fill_neighbour(int i, int j) {

	for(int a = i-1 ; a < i+2 ; a++)
	{
		for(int b = j-1 ; b < j+2 ; b++)
		{
			if (!(a<0 || b<0 || a>=height || b>=breadth || (a==i && b==j)))
			{
				if(grid[a][b] == '#')
				{
					grid[a][b] = '1';
				}	
				else if(grid[a][b] != 'M') grid[a][b] = (char) ((int)grid[a][b] + 1 );
			}
		}
	}

}
	 	
